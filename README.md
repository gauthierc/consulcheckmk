# ConsulCheckMK

This program allows to integrate the status of Consul services in CheckMK.

## Overview

The Consul service catalog knows in real time the list of services hosted on each node and their status. In order to avoid adding an unnecessary load on our services with an additional check from the monitoring server, we wrote a small program in the Go language that queries the Consul API and formats the result with the piggyback syntax of CheckMK.
This allows us to easily integrate the state of the services in our supervision and to have only one tool to consult to know the state of our infrastructure.

Example of piggyback output for the vault server:

```
<<<<vault.example.com>>>>
<<<local>>>
0 CONSUL_serfHealth test=0 OK - Serf Health Status OK
0 CONSUL_vault test=0 OK - Service 'vault' check OK
0 CONSUL_vault-sealed-check test=0 OK - Vault Sealed Status OK<<<>>>
```

The services in Consul can be in 3 states: passing, warning and critical. So we can easily associate them with the value 0, 1 or 2 in CheckMK.
It is also important to note that we had to add the full DNS name of the server in the node agent metadata. This is because the node name in Consul should not contain a "." and does not necessarily have the same name as the server. So we added the fqdn field that allows us to link the Consul node name with the server name in CheckMK.
