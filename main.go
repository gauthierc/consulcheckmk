package main

import (
	"fmt"
	"os"
	"strings"
	"text/template"

	"github.com/hashicorp/consul/api"
)

type Service struct {
	Name      string
	CheckID   string
	NumStatus int
	NagStatus string
}

type Node struct {
	Fqdn    string
	Service map[string]Service
}

// template pour checkMK
const TmplCmk = `
<<<<{{.Fqdn}}>>>>
<<<local>>>
{{range .Service}}{{.NumStatus}} {{.CheckID}} test=0 {{.NagStatus}} - {{.Name}} {{.NagStatus}}
{{end}}<<<<>>>>`

// CmkOut envoi result sur la sortie standard
func CmkOut(r map[string]Node) error {
	tmpl, err := template.New("Node").Parse(TmplCmk)
	if err != nil {
		return err
	}
	for _, v := range r {
		if err = tmpl.Execute(os.Stdout, v); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	// Connexion à l'API Consul
	client, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		panic(err)
	}
	// Récupération de l'ensemble des services quelque soit leur état.
	hchecks, _, err := client.Health().State("any", nil)
	if err != nil {
		panic(err)
	}
	result := make(map[string]Node)
	// Parcours la liste des services
	for _, hc := range hchecks {
		id := strings.LastIndex(hc.CheckID, ":") + 1
		svc := make(map[string]Service)
		checkid := fmt.Sprintf("CONSUL_%s", hc.CheckID[id:])
		switch hc.Status {
		case "passing":
			svc[hc.CheckID] = Service{Name: hc.Name, CheckID: checkid, NumStatus: 0, NagStatus: "OK"}
		case "warning":
			svc[hc.CheckID] = Service{Name: hc.Name, CheckID: checkid, NumStatus: 1, NagStatus: "WARN"}
		case "critical":
			svc[hc.CheckID] = Service{Name: hc.Name, CheckID: checkid, NumStatus: 2, NagStatus: "CRIT"}
		default:
			svc[hc.CheckID] = Service{Name: hc.Name, CheckID: checkid, NumStatus: -1, NagStatus: "UNKNOWN"}
		}

		// Création du noeud s'il n'existe pas
		if _, ok := result[hc.Node]; !ok {
			node, _, err := client.Catalog().Node(hc.Node, nil)
			if err != nil {
				panic(err)
			}
			// Si la métadonnées fqdn est positionnée, on l'utilise
			nodefqdn := node.Node.Meta["fqdn"]
			if nodefqdn == "" {
				nodefqdn = hc.Node
			}
			result[hc.Node] = Node{nodefqdn, svc}
		} else {
			// Ajout du service sur le noeud
			result[hc.Node].Service[hc.CheckID] = svc[hc.CheckID]
		}
	}
	if err = CmkOut(result); err != nil {
		panic(err)
	}
}
